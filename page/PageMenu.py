# -*- coding: utf8 -*-
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

class PageMenu(object):
    _locators = {
        'open_multiple_windows': "//ul[@class='responsive-tabs']/li[4]/a",
        'frame': "//*[@id='example-1-tab-4']//iframe",
        'link': "//div/p/a"
    }

    def __find_button(self, driver, path_element):
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
            By.XPATH, path_element)))
        return driver.find_element_by_xpath(path_element)

    def __find_element(self, driver, path_element):
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.XPATH, path_element)))
        return driver.find_element_by_xpath(path_element)

    def click_open_multiple_windows(self, driver):
        self.__find_button(
            driver, self._locators['open_multiple_windows']).click()

    def set_frame(self, driver):
        frame = self.__find_element(
            driver, self._locators['frame'])
        driver.switch_to_frame(frame)

    def click_link(self, driver):
        link = self.__find_button(driver, self._locators['link']).click()
        return link

    def move_focus(self, driver):
        actions = ActionChains(driver)
        actions.send_keys(Keys.LEFT_CONTROL + Keys.TAB).perform()


