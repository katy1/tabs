from selenium import webdriver
import unittest
from page.PageMenu import PageMenu
from page.PageMain import PageMain
from MethodMain import MethodMain


class TestCase(unittest.TestCase):
    def setUp(self):
        self.page_menu = PageMenu()
        self.page_main = PageMain()
        self.page_method_main = MethodMain()
        self.driver = webdriver.Firefox()

    def test_cursor(self):
        driver = self.driver
        driver.get("http://way2automation.com/way2auto_jquery/frames-and-windows.php")
        driver.switch_to_window(driver.window_handles[-1])
        self.page_method_main.authorization(self.driver)
        self.driver.refresh()
        self.page_main.click_menu_frames_and_windows(driver)
        self.page_menu.click_open_multiple_windows(driver)
        self.page_menu.set_frame(driver)
        self.page_menu.click_link(driver)
        self.page_menu.move_focus(driver)
        driver.switch_to_window(driver.window_handles[-3])
        self.page_menu.click_link(driver)
        driver.switch_to_window(driver.window_handles[-1])
        url = "http://way2automation.com/way2auto_jquery/frames-windows/defult4-window1.html#"
        self.assertEquals(url, driver.current_url)

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
